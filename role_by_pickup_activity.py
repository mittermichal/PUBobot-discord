import time
from modules import utils, stats3, config
import discord
import argparse
import datetime


class MyClient(discord.Client):

    def __init__(self, options, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.options = options
        if options['time_gap']:
            self.since = int(time.time()) - utils.parse_time_input(options['time_gap'])
            print(f"since: {datetime.datetime.fromtimestamp(self.since).strftime('%c')}")
        else:
            print(f"since: {options['since'].strftime('%c')}")
            self.since = options['since'].timestamp()

    async def on_ready(self):
        print('discord client logged in')

        channel = self.get_channel(self.options['channel_id'])
        if channel is None:
            raise Exception("channel not found")
        print("channel:", channel.name)

        guild = self.get_guild(channel.guild.id)
        if guild is None:
            raise Exception("guild not found")
        print("guild:", guild.name)

        role = guild.get_role(self.options['role_id'])
        if role is None:
            raise Exception("role not found")
        print("role:", role.name)

        # TODO: remove role from everyone
        to_remove_role = set(role.members)

        stats3.init()
        if self.options['min_matches'] is None:
            # top x
            top = stats3.c.execute(
                "SELECT user_id,count(user_id) as count, user_name FROM player_pickups "
                "WHERE channel_id = ? and at > ? "
                "GROUP BY user_id ORDER by count(user_id) DESC LIMIT ?",
                (self.options['channel_id'], self.since, self.options['player_limit']))
        else:
            # use min_matches
            top = stats3.c.execute(
                "SELECT user_id,count(user_id) as count, user_name FROM player_pickups "
                "WHERE channel_id = ? and at > ?"
                "GROUP BY user_id HAVING count(user_id) >= ? ORDER by count(user_id) DESC",
                (self.options['channel_id'], self.since, self.options['min_matches']))

        for row in top:
            user_id = row['user_id']
            try:
                member = guild.get_member(int(user_id))
                print(f"{member.display_name} matches count: {row['count']}")
                to_remove_role.discard(member)
                if role not in member.roles:
                    await member.add_roles(role, reason="role by pickup activity script")
            except Exception as e:
                print(f"ERROR: {e} user_id: {user_id} nick: {row['user_name']}", user_id)
                continue

        for member in to_remove_role:
            await member.remove_roles(role, reason="role by pickup activity script")
            print("role removed from:", member.display_name)

        await self.close()

    # async def on_error(self, event_method, *args, **kwargs):
    #     await self.close()


def parse_time_gap(v):
    try:
        return utils.parse_time_input(v)
    except Exception as e:
        raise argparse.ArgumentTypeError(str(e))


def positive_int(v):
    try:
        v = int(v)
    except ValueError:
        raise argparse.ArgumentTypeError('Positive integer value expected.')
    if v <= 0:
        raise argparse.ArgumentTypeError('Positive integer value expected.')
    return v


def iso_datetime(v):
    try:
        return datetime.datetime.fromisoformat(v)
    except ValueError as e:
        raise argparse.ArgumentTypeError(e)


parser = argparse.ArgumentParser(description='adds role to given number of top players from channel')
parser.add_argument('channel_id', metavar='channel id', type=int)
parser.add_argument('role_id', metavar='role id', type=int, help='role id to be assigned')
parser.add_argument('--player-limit', metavar='player limit', type=positive_int, help='how many players from !top')
parser.add_argument('--time-gap', metavar='time gap', nargs='+', help='top of X time. example: 10d 12h 30m')
parser.add_argument('--min-matches', type=int)
parser.add_argument('--since', type=iso_datetime, help='since isotime. example: 2021-12-24')

arguments = parser.parse_args()

if not arguments.since and not arguments.time_gap:
    parser.error('specify either `--time_gap` or `--since`')

if not arguments.player_limit and not arguments.min_matches:
    parser.error('specify either `--player-limit` or `--min-matches`')

intents = discord.Intents.default()
intents.members = True

client = MyClient(
    arguments.__dict__,
    intents=intents
)

config.init()
client.run(config.cfg.DISCORD_TOKEN)
