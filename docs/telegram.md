# Telegram notifications

User can set up notifications from GatherMaster to be sent to his telegram account. Currently users will get Telegram message when match is in waiting ready stage and they haven't readied up after 15 seconds. Bot doesn't use or store any phone numbers.

## Set up steps:
1. DM `!tg` to discord bot. Bot will reply with link to telegram bot and token to be sent to discord.
2. Click on the telegram bot link and send token to Telegram bot e.g.: `/token 123456`
3. DM `!tg` to discord bot again. If succesful you will receive message from Telegram bot.

### Commands:
- `!tg` - integration setup
- `!tg disable` - disables notifications
- `!tg enable` - enables notifications
- `!tg test` - send test message to Telegram, if integration is set up
- `!tg remove` - removes user integration from database, `!tg` to setup again.