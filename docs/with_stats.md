# `!with_stats` examples

## `!with_stats @player1 @player2`:

```
pickup      is player1 with player2 in team    result    count
--------  ---------------------------------  --------  -------
6vs6                                     ⚔️      L          8
6vs6                                     ⚔️      D          2
6vs6                                     ⚔️      W         13
6vs6                                     🤝      L         16
6vs6                                     🤝      W         14
```
Rows explanation:
- player1 lost against player2 8 times
- player1 draw against player2 2 times
- player1 won against player2 13 times
- player1 lost with player2 16 times
- player1 won with player2 14 times

## `!with_stats @player1 @player2 @player3`:

```
pickup      is player1 with player2 in team    is player1 with player3 in team    result    count
--------  ---------------------------------  ---------------------------------  --------  -------
6vs6                                     ⚔️                                 ⚔️        W        1
6vs6                                     ⚔️                                 🤝        L        2
6vs6                                     ⚔️                                 🤝        W        1
6vs6                                     🤝                                 ⚔️        L        2
6vs6                                     🤝                                 ⚔️        W        4
6vs6                                     🤝                                 🤝        L        1
6vs6                                     🤝                                 🤝        W        2
```