import threading
from typing import Optional

import client_config
from sentry_sdk import capture_exception
import dataclasses

import modules.events_json

if hasattr(client_config, 'WEB_URL'):
    import requests
if client_config.IPC_ENABLED:
    from modules import ipc
if client_config.POST_EVENTS:
    import requests

# TODO: use observer pattern? https://stackoverflow.com/a/2022629

# TODO: subscribe to:
# player added, removed
# match started
# match state change
# match pick_step change
# match map_pick_order change, !put, !subfor, !capfor, !pick_captains or just alpha/beta players?
# match ready_use
# match alpha/beta draw
# match players_ready change
# user role change, role permission change -> reload pickups state
# !noadd !forgive
# party invite, challenge, list change, party player change
# rank change?
# expire change
# on channel message for read only channel? could also send messages as bot account

@dataclasses.dataclass(unsafe_hash=True)
class Change:
    call: str
    attribute: str
    before: any
    before_len: Optional[int] = None

def post_to_web(endpoint, json, **kwargs):
    if not hasattr(client_config, 'WEB_URL'):
        return
    try:
        requests.post(client_config.WEB_URL + endpoint, json=json, verify=False)
    except Exception as e:
        capture_exception(e)
        pass


def fire_and_forget(endpoint, json, **kwargs):
    if client_config.IPC_ENABLED:
        threading.Thread(target=post_to_web, args=(endpoint, json), kwargs=kwargs).start()


def fire_and_forget_external(url, json=None, headers=None):
    if not client_config.POST_EVENTS:
        return
    def call():
        try:
            requests.post(url, json=json, headers=headers)
        except Exception as e:
            capture_exception(e)
            pass
    threading.Thread(target=call).start()


def handle_post_event_extension(pickup, data):
    post_events = pickup.channel.get_cfg_extension('POST_EVENTS', pickup)
    if post_events:
        if isinstance(post_events, list):
            for post_event_params in post_events:
                fire_and_forget_external(post_event_params.get('url'), data, post_event_params.get('headers', None))
        else:
            fire_and_forget_external(post_events.get('url'), data, post_events.get('headers', None))

def pickup_change(pickup, change: Change):
    if client_config.IPC_ENABLED:
        fire_and_forget("/events", {"event": "pickup_change", "pickup": modules.events_json.pickup_change_json(pickup)})
    handle_post_event_extension(pickup, {
                "event": "pickup_change",
                "pickup": modules.events_json.pickup_change_json(pickup),
                "change": change.__dict__
            })


def match_change(match, change: Change):
    if client_config.IPC_ENABLED:
        fire_and_forget("/events", {"event": "match_change", "match": modules.events_json.match_json(match)})
    handle_post_event_extension(match.pickup, {
                "event": "match_change",
                "match": modules.events_json.match_json(match),
                "change": change.__dict__
            })


def map_pick(match, change: Change):
    if client_config.IPC_ENABLED:
        fire_and_forget("/events", {"event": "map_pick", "match": modules.events_json.map_pick_json(match)})
    handle_post_event_extension(match.pickup,{
                "event": "map_pick",
                "match": modules.events_json.map_pick_json(match),
                "change": change.__dict__
            })


def player_pick(match, change: Change):
    if client_config.IPC_ENABLED:
        fire_and_forget("/events", {"event": "player_pick", "match": modules.events_json.player_pick_json(match)})
    handle_post_event_extension(match.pickup,{
                "event": "player_pick",
                "match": modules.events_json.player_pick_json(match),
                "change": change.__dict__
            })


def set_ready(match):
    if client_config.IPC_ENABLED:
        fire_and_forget("/events", {"event": "set_ready", "match": modules.events_json.set_ready_json(match)})
    # if match.pickup.channel.get_cfg_extension('POST_EVENTS', match.pickup):
    #     fire_and_forget_external(
    #         match.pickup.channel.get_cfg_extension('POST_EVENTS', match.pickup).get('url'),
    #         {"event": "set_ready", "match": modules.events_json.set_ready_json(match)},
    #         match.pickup.channel.get_cfg_extension('POST_EVENTS', match.pickup).get('headers', None)
    #     )
